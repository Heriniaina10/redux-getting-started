'use strict';

var express = require('express');
var app = express();
var path = require('path');

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});


const port = 3000;
app.listen(port, () => console.log(`App is listening on port: ${port}`));