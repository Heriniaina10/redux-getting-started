'use strict';

import { createStore } from 'redux';

const POST_BOOK = 'POST_BOOK';
const DECREMENT = 'DECREMENT';

//STEP 3 define reducers
const reducers = (state={books: []}, action) => {
    switch(action.type) {
        case POST_BOOK:
            return {books: [...state.books, ...action.payload]};
    }

    return state;
}

// STEP 1 create Store
const store = createStore(reducers);
store.subscribe(() => console.log('Current: ', store.getState()));

//STEP 2 create and dispatch actions
const actionPost = {
    type: POST_BOOK,
    payload: [
        {
            id: 1,
            title: 'Guerre de Refidim',
            price: 17
        }, {
            id: 2,
            title: 'Guerre de Jericho',
            price: 6
        }
    ]
};

const actionPost2 = {
    type: POST_BOOK,
    payload: [{
            id: 3,
            title: 'Guerre d\'Ay',
            price: 7
        }
    ]
};

store.dispatch(actionPost);
store.dispatch(actionPost2);